using System.Text.Json.Serialization;

namespace tech_test_payment_api.DTO
{
    public class ItemDTO
    {
        [JsonIgnore]
        public int Id { get; set; }
        public string CodSistema { get; set; }
        public string Nome { get; set; }
    }
}