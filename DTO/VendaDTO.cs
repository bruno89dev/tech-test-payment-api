using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using tech_test_payment_api.Models;
using tech_test_payment_api.Models.Enum;

namespace tech_test_payment_api.DTO
{
    public class VendaDTO
    {
        [JsonIgnore]
        public int Id { get; set; }
        public string CodVenda { get; set; }
        public DateTime Data { get; set; }
        public int ItemId { get; set; }
        [JsonIgnore]
        public List<ItemDTO> Itens { get; set; }
        public int VendedorId { get; set; }
        [JsonIgnore]
        public Vendedor Vendedor { get; set; }
        public StatusVenda Status { get; set; }
    }
}