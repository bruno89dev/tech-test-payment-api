using System.Text.Json.Serialization;

namespace tech_test_payment_api.DTO
{
    public class VendedorDTO
    {
        [JsonIgnore]
        public int Id { get; set; }
        public string Nome { get; set; }
        public string CodSistema { get; set; }
        public string Cpf { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
    }
}