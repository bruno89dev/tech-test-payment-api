namespace tech_test_payment_api.Models.Enum
{
    public enum StatusVenda
    {
        AguardandoPagamento = 1,
        PagamentoAprovado = 2,
        EnviadaParaTransportadora = 3,
        Entregue = 4,
        Cancelada = 5
    }
}