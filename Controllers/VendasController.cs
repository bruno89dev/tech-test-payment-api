
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Models.Enum;
using tech_test_payment_api.Services;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VendasController : ControllerBase
    {
        private VendasService _vendasService;

        public VendasController(VendasService vendasService) {
            _vendasService = vendasService;
        }

        [HttpGet("Listar")]
        public IActionResult Listar() {
            var listaVendas = _vendasService.Listar();
            if (listaVendas.Count > 0)
                return Ok(listaVendas);
            return NotFound("Nenhuma venda encontrada");
        }

        [HttpGet("BuscarPorCodVenda")]
        public IActionResult BuscarPorCodVenda(string codVenda) {
            var vendaPorCod = _vendasService.BuscarPorCodVenda(codVenda);
            if (vendaPorCod.Count > 0)
                return Ok(vendaPorCod);
            return NotFound($"Nenhuma venda encontrada com o código {codVenda}");
        }

        [HttpGet("BuscaPorStatus")]
        public IActionResult BuscarPorStatus(StatusVenda status) {
            var vendaPorStatus = _vendasService.BuscarPorStatusVenda(status);
            if (vendaPorStatus.Count > 0)
                return Ok(vendaPorStatus);
            return NotFound($"Nenhuma venda encontrada com o status {status}");
        }

        [HttpPost("NovaVenda")]
        public IActionResult NovaVenda(VendaDTO vendaDTO, int id) {
            var venda = _vendasService.CriarVenda(vendaDTO, id);
            Response.StatusCode = 201;
            return new ObjectResult(new {info = $"Venda adicionada com sucesso"});
        }

        [HttpPost("AtualizarStatus_PagamentoAutorizado")]
        public IActionResult AutorizarPagamento(string codVenda) {
            var venda = _vendasService.AutorizarPagamento(codVenda);
            return Ok(venda);
        }

        [HttpPost("AtualizarStatus_EnviadaParaTransportadora")]
        public IActionResult EnviarTransportadora(string codVenda) {
            var venda = _vendasService.EnviarParaTransportadora(codVenda);
            return Ok(venda);
        }
        
        [HttpPost("AtualizarStatus_Entregue")]
        public IActionResult EntregarPedido(string codVenda) {
            var venda = _vendasService.Entregar(codVenda);
            return Ok(venda);
        }
        
        [HttpPost("AtualizarStatus_Cancelada")]
        public IActionResult CancelarPedido(string codVenda) {
            var venda = _vendasService.Cancelar(codVenda);
            return Ok(venda);
        }

        [HttpPut("Editar")]
        public IActionResult Editar(VendaDTO vendaDTO) {
            var venda = _vendasService.Editar(vendaDTO);
            return Ok(venda);
        }

        [HttpDelete("Excluir/{id}")]
        public IActionResult Excluir(int id) {
            var venda = _vendasService.Excluir(id);
            if (venda is not null)
                return Ok($"Venda excluída com sucesso");
            return NotFound($"Nenhuma venda encontrada com o id {id}");
        }
    }
}