using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Services;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VendedoresController : ControllerBase
    {
        private VendedoresService _vendedoresService;

        public VendedoresController(VendedoresService vendedoresService) {
            _vendedoresService = vendedoresService;
        }

        [HttpGet("Listar")]
        public IActionResult Listar() {
            var listaVendedores = _vendedoresService.Listar();
            if (listaVendedores.Count > 0)
                return Ok(listaVendedores);
            return NotFound("Nenhum(a) vendedor(a) encontrado(a)");
        }

        [HttpGet("BuscarPorCodSistema")]
        public IActionResult BuscarPorId(string codSistema) {
            var itemPorId = _vendedoresService.BuscarPorCodSistema(codSistema);
            if (itemPorId is not null)
                return Ok(itemPorId);
            return NotFound($"Nenhum(a) vendedor(a) encontrado(a) com o código {codSistema}");
        }

        [HttpGet("BuscarPorCpf")]
        public IActionResult BuscarPorCpf(string cpf) {
            var vendedorPorCpf = _vendedoresService.BuscarPorCpf(cpf);
            if (vendedorPorCpf is not null)
                return Ok(vendedorPorCpf);
            return NotFound($"Nenhum(a) vendedor(a) encontrado(a) com o CPF {cpf}");
        }

        [HttpPost("Cadastrar")]
        public IActionResult Cadastrar(VendedorDTO vendedorDTO) {
            var vendedor = _vendedoresService.CriarVendedor(vendedorDTO);
            Response.StatusCode = 201;
            return new ObjectResult(new {info = $"Novo(a) vendedor(a) - {vendedor.Nome} - adicionado(a) com sucesso"});
        }

        [HttpPut("Editar")]
        public IActionResult Editar(VendedorDTO vendedorDTO) {
            var vendedor = _vendedoresService.Editar(vendedorDTO);
            return Ok(vendedor);
        }

        [HttpDelete("Excluir/{id}")]
        public IActionResult Excluir(int id) {
            var vendedor = _vendedoresService.Excluir(id);
            if (vendedor is not null)
                return Ok($"Vendedor(a) {vendedor.Nome} excluído(a) com sucesso");
            return NotFound($"Nenhum(a) vendedor(a) encontrado(a) com o id {id}");
        }
    }
}