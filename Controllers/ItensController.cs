using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Services;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ItensController : ControllerBase
    {
        private ItensService _itensService;

        public ItensController(ItensService itensService) {
            _itensService = itensService;
        }

        [HttpGet("Listar")]
        public IActionResult Listar() {
            var listaItens = _itensService.Listar();
            if (listaItens.Count > 0)
                return Ok(listaItens);
            return NotFound("Nenhum item encontrado");
        }

        [HttpGet("BuscarPorCodSistema")]
        public IActionResult BuscarPorCodSistema(string codSistema) {
            var itemPorCodSistema = _itensService.BuscarPorCodSistema(codSistema);
            if (itemPorCodSistema is not null)
                return Ok(itemPorCodSistema);
            return NotFound($"Nenhum item encontrado com o código {codSistema}");
        }

        [HttpPost("Cadastrar")]
        public IActionResult Cadastrar(ItemDTO itemDTO) {
            var item = _itensService.CriarItem(itemDTO);
            Response.StatusCode = 201;
            return new ObjectResult(new {info = $"Novo item - {item.Nome} - adicionado com sucesso"});
        }

        [HttpPut("Editar")]
        public IActionResult Editar(ItemDTO itemDTO) {
            var item = _itensService.Editar(itemDTO);
            return Ok(item);
        }

        [HttpDelete("Excluir/{id}")]
        public IActionResult Excluir(int id) {
            var item = _itensService.Excluir(id);
            if (item is not null)
                return Ok($"Item {item.Nome} excluído com sucesso");
            return NotFound($"Nenhum item encontrado com o id {id}");
        }
    }
}