using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using tech_test_payment_api.Data;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Models;
using tech_test_payment_api.Models.Enum;

namespace tech_test_payment_api.Services
{
    public class VendasService
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly ItensService _itensService;

        public VendasService(ApplicationDbContext context, IMapper mapper, ItensService itensService) {
            _context = context;
            _mapper = mapper;
            _itensService = itensService;
        }

        public Venda CriarVenda(VendaDTO vendaDTO, int idVendedor)
        {
            var venda = _mapper.Map<Venda>(vendaDTO);
            var vendedor = _context.Vendedores.Where(v => v.Id == idVendedor).FirstOrDefault();
            venda.ListaItens = _itensService.AdicionarItens(vendaDTO.Itens);
            venda.Vendedor = vendedor;
            venda.Status = Models.Enum.StatusVenda.AguardandoPagamento;
            _context.Vendas.Add(venda);
            _context.SaveChanges();
            return venda;
        }

        public List<Venda> Listar() {
            return _context.Vendas.ToList();
        }

        public List<Venda> BuscarPorCodVenda(string codVenda) {
            return _context.Vendas.Where(v => v.CodVenda == codVenda).ToList();
        }

        public List<Venda> BuscarPorStatusVenda(StatusVenda status) {
            return _context.Vendas.Where(v => v.Status == status).ToList();
        }

        public Venda Editar(VendaDTO vendaDTO) {
            var venda = _mapper.Map<Venda>(vendaDTO);
            _context.Vendas.Update(venda);
            _context.SaveChanges();
            return venda;
        }

        public Venda Excluir(int id) {
            var venda = _context.Vendas.Where(v => v.Id == id).FirstOrDefault();
            _context.Vendas.Remove(venda);
            _context.SaveChanges();
            return venda;
        }

        public List<Venda> AutorizarPagamento(string codVenda) {
            var listaVendas = _context.Vendas.Where(v => v.CodVenda == codVenda).ToList();
            if (listaVendas.Count > 0)
                foreach(Venda venda in listaVendas) {
                    if(venda.Status == Models.Enum.StatusVenda.AguardandoPagamento)
                        venda.Status = Models.Enum.StatusVenda.PagamentoAprovado;
                        _context.Vendas.Update(venda);
                        _context.SaveChanges();
                }
            return listaVendas;
        }

        public List<Venda> EnviarParaTransportadora(string codVenda) {
            var listaVendas = _context.Vendas.Where(v => v.CodVenda == codVenda).ToList();
            if (listaVendas.Count > 0)
                foreach(Venda venda in listaVendas) {
                    if(venda.Status == Models.Enum.StatusVenda.PagamentoAprovado)
                        venda.Status = Models.Enum.StatusVenda.EnviadaParaTransportadora;
                        _context.Vendas.Update(venda);
                        _context.SaveChanges();
                }
            return listaVendas;
        }

        public List<Venda> Entregar(string codVenda) {
            var listaVendas = _context.Vendas.Where(v => v.CodVenda == codVenda).ToList();
            if (listaVendas.Count > 0)
                foreach(Venda venda in listaVendas) {
                    if(venda.Status == Models.Enum.StatusVenda.EnviadaParaTransportadora)
                        venda.Status = Models.Enum.StatusVenda.Entregue;
                        _context.Vendas.Update(venda);
                        _context.SaveChanges();
                }
            return listaVendas;
        }

        public List<Venda> Cancelar(string codVenda) {
            var listaVendas = _context.Vendas.Where(v => v.CodVenda == codVenda).ToList();
            if (listaVendas.Count > 0)
                foreach(Venda venda in listaVendas) {
                    if(venda.Status == Models.Enum.StatusVenda.AguardandoPagamento || venda.Status == Models.Enum.StatusVenda.PagamentoAprovado)
                        venda.Status = Models.Enum.StatusVenda.Cancelada;
                        _context.Vendas.Update(venda);
                        _context.SaveChanges();
                }
            return listaVendas;
        }
    }
}